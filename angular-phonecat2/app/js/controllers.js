'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
  function($scope, Phone) {
    $scope.phones = Phone.query();
  }]).directive('myCustomer', function() {
    return {
//	  transclude: true,	
			scope: {
				phones: '=info'
			},
			link: function (scope, element, attrs) {
				scope.add = function() {
					scope.phones.push({age:scope.phones.length, id:scope.phones.length+1, snippet:scope.itemSnip, name: scope.itemName, imageUrl:'img/phones/no_image.gif'});
				},
				scope.remove=function(item){ 
					var index=scope.phones.indexOf(item)
					scope.phones.splice(index,1);        
				} 
			},
      templateUrl: 'list.html'
    };
  });

phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);
